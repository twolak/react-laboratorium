import React from "react";

export default function SearchBar() {
  return (
    <div className="input-group" style={{ marginTop: "1.5rem" }}>
      <form className="mx-2 my-auto d-inline w-100" role={"search"}>
        <div className="input-group border border-primary rounded">
          <span className="input-group-append">
            <button className="btn btn-outline rounded-pill" id="search-button">
              <i className="bi bi-search"></i>
            </button>
          </span>
          <input
          id="searchInput"
            className="form-control me-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            style={{ border: "none" }}
          />
          <span className="input-group-append mx-2">
            <img
              src="https://demos.themeselection.com/sneat-bootstrap-html-admin-template-free/assets/img/avatars/1.png"
              alt="profile_pic"
              className="rounded-circle img-responsive"
              style={{ width: "2vw", height: "auto" }}
            />
          </span>
        </div>
      </form>
    </div>
  );
}
