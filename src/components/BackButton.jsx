import React, { Fragment } from "react";
import { useNavigate } from "react-router";

export default function BackButton() {
  const navigate = useNavigate();
  const handleBack = () => {
    navigate(-1);
  };
  return (
    <Fragment>
      <button className="btn btn-secondary" onClick={handleBack}>
        {" "}
        Go back
      </button>
    </Fragment>
  );
}
