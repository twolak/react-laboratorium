import React from "react";

export default function ChatMessage({ message }) {
  return <div className={`chat-message chat-message-${message.source}`}>{message.text}</div>;
}
