import React from "react";
import ChatWindow from "./ChatWindow";
import ChatToggleButton from "./ChatToggleButton";

export default function ChatComponent({ show, onClick }) {
  return (
    <div className="chat-container">
      {show && (
        <div>
          <div>
            <i
              className="btn bi bi-x"
              style={{
                color: "black",
                float: "right",
                position: "absolute",
                left: "90%",
              }}
              onClick={onClick}
            ></i>
          </div>
          <br />
          <ChatWindow />{" "}
        </div>
      )}
      {!show && <ChatToggleButton onClick={onClick} />}
    </div>
  );
}
