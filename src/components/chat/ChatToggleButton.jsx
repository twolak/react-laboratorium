import React from "react";

export default function ChatToggleButton({ onClick }) {
  return (
    <div className="chat-toggle">
      <div
        className="btn"
        style={{
          borderRadius: "1em",
          backgroundColor: "#0d6efd",
        }}
        onClick={onClick}
      >
        {" "}
        <i className="bi bi-chat-left" style={{ color: "white"}}></i>
      </div>
    </div>
  );
}
