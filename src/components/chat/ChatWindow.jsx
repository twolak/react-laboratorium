import React from "react";
import ChatMessage from "./ChatMessage";

const exampleMessage1 = { text: "Siema!!", source: "sent" };
const exampleMessage2 = { text: "Siema!!", source: "received" };

export default function ChatWindow({ onClick }) {
  return (
    <div className="chat-window">
      <ChatMessage message={exampleMessage1} />
      <br />
      <br />
      <ChatMessage message={exampleMessage2} />
    </div>
  );
}
