import React, { useState } from "react";
import { useNavigate } from "react-router";

export default function LoginComponent() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const handleLogin = () => {
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);
    navigate("/");
  };
  return (
    <form className="login-form">
      <div className="mb-3">
        <label>Login: </label>{" "}
        <input
          type="text"
          value={username}
          onChange={handleUsernameChange}
          required
        />
      </div>
      <div className="mb-3">
        <label>Password: </label>{" "}
        <input
          type="password"
          value={password}
          onChange={handlePasswordChange}
          required
        />
      </div>
      <div className="mb-3">
        <div className="btn btn-primary" onClick={handleLogin}>
          <span>Log in</span>
        </div>
      </div>
    </form>
  );
}
