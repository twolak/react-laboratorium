import React from "react";
import { useNavigate } from "react-router-dom";
import MenuSubItem from "./MenuSubItem";

export default function MenuItem(props) {
  const { iconClass, title, active, subItems, index, routeLink } = props.item;
  const navigate = useNavigate();
  const handleClick = (link) => {
    if (link) {
      navigate(link);
    }
  };
  return (
    <li
      className="nav-item m-0"
      onClick={() => {
        handleClick(routeLink);
      }}
    >
      <a
        href={`#collapse${index}`}
        data-bs-toggle="collapse"
        role="button"
        className={`btn btn-primary m-1 p-2 nav-link ${active ? "active" : ""}`}
      >
        <i className={iconClass}></i>
        <span> {title}</span>
      </a>
      <div
        className="collapse"
        id={`collapse${index}`}
        style={{ justifyContent: "start" }}
      >
        {subItems.length > 0 && (
          <ul className="nav nav-pills flex-column mb-auto">
            {subItems.map((subItem, idx) => {
              return (
                <span key={idx}>
                  <li
                    className="nav-item"
                    style={{ display: "inline" }}
                    onClick={(e) => {
                      e.preventDefault();
                      handleClick(subItem.routeLink);
                    }}
                  >
                    <MenuSubItem item={subItem} />
                  </li>
                </span>
              );
            })}
          </ul>
        )}
      </div>
    </li>
  );
}
