import React from "react";
import BackButton from "../BackButton";

export default function WithoutMenu() {
  return (
    <div>
      WithoutMenu works!
      <BackButton />
    </div>
  );
}
