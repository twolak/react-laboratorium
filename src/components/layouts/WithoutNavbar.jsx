import React from "react";
import BackButton from "../BackButton";

export default function WithoutNavbar() {
  return (
    <div>
      WithoutNavbar works!
      <BackButton />
    </div>
  );
}
