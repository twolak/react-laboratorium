import React from "react";
import BackButton from "../BackButton";

export default function WithContainer() {
  return (
    <div>
      WithContainer works!
      <BackButton />
    </div>
  );
}
