import React from "react";
import BackButton from "../BackButton";

export default function WithoutContainer() {
  return (
    <div>
      WithoutContainer works!
      <BackButton />
    </div>
  );
}
