import React, { useEffect, useState } from "react";

export default function MenuSubItem(props) {
  const { item } = props;

  return (
    <a href="" className="btn m-1 p-1 nav-link" style={{ display: "inline" }}>
      <span>{item.title}</span>
    </a>
  );
}
