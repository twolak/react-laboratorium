import React, { useContext } from "react";
import SearchBar from "./SearchBar";
import { FontContext, FontDispatchContext } from "../context/FontContext";

export default function Dashboard(props) {
  const { onThemeChange, theme } = props;
  const context = useContext(FontContext);

  const handlePlusClick = () => {
    if (context.fontSize < 2) {
      dispatch(parseFloat((context.fontSize + 0.1).toFixed(1)));
    }
  };
  const handleMinusClick = () => {
    if (context.fontSize >= 0.1) {
      dispatch(parseFloat((context.fontSize - 0.1).toFixed(1)));
    }
  };

  const dispatch = useContext(FontDispatchContext);

  return (
    <div className="container-fluid content">
      <div className="row">
        <div
          className="col-1"
          style={{
            position: "relative",
            top: "2vh",
          }}
        >
          <button
            id="theme-button"
            className="btn btn-lg"
            onClick={onThemeChange}
          >
            <i
              className={
                theme === "light"
                  ? "bi bi-brightness-high-fill"
                  : "bi bi-moon-stars-fill"
              }
            ></i>
          </button>
        </div>
        <div className="col-11">
          <SearchBar />
        </div>
      </div>
      <br />
      <div className="row">
        <div>
          <span>Font size</span>
          <button className="btn btn-success" onClick={handlePlusClick}>
            +
          </button>
          <button className="btn btn-danger" onClick={handleMinusClick}>
            -
          </button>
        </div>
      </div>
      <div className="row">
        <div className="col-6">
          {/* CongratulationsCard START*/}
          <div className="card">
            <div className="card-body"></div>
          </div>
          {/* CongratulationsCard STOP*/}
        </div>
        <div className="col-3">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
        <div className="col-3">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-6">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
        <div className="col-6">
          <div className="row">
            <div className="col-6">
              <div className="card">
                <div className="card-body"></div>
              </div>
            </div>
            <div className="col-6">
              <div className="card">
                <div className="card-body"></div>
              </div>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-12">
              <div className="card">
                <div className="card-body"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-4">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body"></div>
          </div>
        </div>
      </div>
    </div>
  );
}
