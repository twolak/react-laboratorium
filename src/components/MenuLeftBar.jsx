import React, { useEffect, useState } from "react";
import MenuItem from "./MenuItem";

export default function MenuLeftBar() {
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const [mainItems, setMainItems] = useState([
    {
      title: "Dashboard",
      iconClass: "bi bi-house",
      active: true,
      index: 0,
      subItems: [],
      routeLink: "/",
    },
    {
      title: "Layouts",
      iconClass: "bi bi-columns",
      active: false,
      index: 1,
      subItems: [
        {
          title: "Without menu",
          routeLink: "/layouts/without-menu",
        },
        {
          title: "Without navbar",
          routeLink: "/layouts/without-navbar",
        },
        {
          title: "Without container",
          routeLink: "/layouts/without-container",
        },
        {
          title: "Container",
          routeLink: "/layouts/container",
        },
      ],
    },
  ]);
  const [accountItems, setAccountItems] = useState([
    {
      title: "Account settings",
      iconClass: "bi bi-person-gear",
      active: false,
      index: 2,
      subItems: [
        {
          title: "Account",
          routeLink: "/account/settings",
        },
        {
          title: "Notifications",
          routeLink: "/account/notifications",
        },
        {
          title: "Connections",
          routeLink: "/account/connections",
        },
      ],
    },
    {
      title: "Authentications",
      iconClass: "bi bi-key",
      active: false,
      index: 3,
      subItems: [],
      routeLink: "/authentications",
    },
    {
      title: "Misc",
      iconClass: "bi bi-inboxes-fill",
      active: false,
      index: 4,
      subItems: [],
      routeLink: "/misc",
    },
  ]);
  const handleClick = (item) => {
    // console.log(`active index: ${index}`);
    setActiveItemIndex(item.index);
  };

  useEffect(() => {
    setMainItems(
      mainItems.map((item) => {
        return {
          ...item,
          active: item.index === activeItemIndex,
        };
      })
    );
    setAccountItems(
      accountItems.map((item) => {
        return {
          ...item,
          active: item.index === activeItemIndex,
        };
      })
    );
  }, [activeItemIndex]);

  return (
    <div className="sidebar">
      <a
        href="/"
        className="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none"
      >
        <svg className="bi me-2" style={{ width: "40px", height: "32px" }}>
          <use xlinkHref="#bootstrap"></use>
        </svg>
        <span className="fs-4">Sidebar</span>
      </a>
      <hr />
      <ul className="nav nav-pills flex-column mb-auto">
        {mainItems.map((item) => (
          <span key={item.index}>
            <MenuItem item={item} />
          </span>
        ))}
        <hr />
        {accountItems.map((item) => (
          <span key={item.index}>
            <MenuItem item={item} />
          </span>
        ))}
      </ul>
    </div>
  );
}
