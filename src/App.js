import "./App.css";
import MenuLeftBar from "./components/MenuLeftBar";
import { Fragment, useContext, useState } from "react";
import Dashboard from "./components/Dashboard";
import { Navigate, Route, Routes } from "react-router";
import WithoutMenu from "./components/layouts/WithoutMenu";
import MiscPage from "./components/misc/MiscPage";
import AuthenticationsPage from "./components/authentications/AuthenticationsPage";
import AccountConnectionsPage from "./components/account/AccountConnectionsPage";
import AccountNotificationPage from "./components/account/AccountNotificationPage";
import AccountSettingsPage from "./components/account/AccountSettingsPage";
import WithoutContainer from "./components/layouts/WithoutContainer";
import WithoutNavbar from "./components/layouts/WithoutNavbar";
import WithContainer from "./components/layouts/WithContainer";
import { FontContext } from "./context/FontContext";
import styled from "styled-components";
import ChatComponent from "./components/chat/ChatComponent";
import { BrowserRouter } from "react-router-dom";
import LoginComponent from "./components/LoginComponent";

const StyledContainer = styled.div`
  * {
    font-size: ${(props) => props.size}rem;
  }
`;

function App() {
  const [theme, setTheme] = useState("light");
  const [showChat, setShowChat] = useState(false);
  const { fontSize } = useContext(FontContext);

  const handleThemeChange = () => {
    setTheme(theme === "light" ? "dark" : "light");
  };
  const handleToggle = () => {
    setShowChat(!showChat);
  };
  return (
    <BrowserRouter>
      <div className={`App ${theme}`} data-bs-theme={theme}>
        <StyledContainer size={fontSize}>
          <Routes>
            <Route
              exact
              path="/"
              Component={() => (
                <Fragment>
                  <MenuLeftBar />
                  <Dashboard onThemeChange={handleThemeChange} theme={theme} />
                </Fragment>
              )}
            />
            <Route path="/login" Component={LoginComponent} />
            <Route
              path="/layouts/without-menu"
              Component={() =>
                localStorage.getItem("username") ? (
                  <WithoutMenu />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/layouts/without-navbar"
              Component={() =>
                localStorage.getItem("username") ? (
                  <WithoutNavbar />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/layouts/without-container"
              Component={() =>
                localStorage.getItem("username") ? (
                  <WithoutContainer />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/layouts/container"
              Component={() =>
                localStorage.getItem("username") ? (
                  <WithContainer />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />

            <Route
              path="/account/settings"
              Component={() =>
                localStorage.getItem("username") ? (
                  <AccountSettingsPage />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/account/notifications"
              Component={() =>
                localStorage.getItem("username") ? (
                  <AccountNotificationPage />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/account/connections"
              Component={() =>
                localStorage.getItem("username") ? (
                  <AccountConnectionsPage />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/authentications"
              Component={() =>
                localStorage.getItem("username") ? (
                  <AuthenticationsPage />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
            <Route
              path="/misc"
              Component={() =>
                localStorage.getItem("username") ? (
                  <MiscPage />
                ) : (
                  <Navigate to={"/login"} />
                )
              }
            />
          </Routes>
          {localStorage.getItem("username") && (
            <ChatComponent show={showChat} onClick={handleToggle} />
          )}
        </StyledContainer>
      </div>
    </BrowserRouter>
  );
}

export default App;
