import { createContext, useReducer } from "react";

export const FontContext = createContext(null);
export const FontDispatchContext = createContext(null);

function fontSizeReducer(state, payload) {
  return {
    ...state,
    fontSize: payload,
  };
}
const initialFontSize = {
  fontSize: 1,
};

export const FontSizeProvider = ({ children }) => {
  const [fontSize, dispatch] = useReducer(fontSizeReducer, initialFontSize);

  return (
    <FontContext.Provider value={fontSize}>
      <FontDispatchContext.Provider value={dispatch}>
        {children}
      </FontDispatchContext.Provider>
    </FontContext.Provider>
  );
};
